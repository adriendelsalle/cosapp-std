from cosapp_std.materials.tools.helpers import param_check, ideal_gas_density


"""
Nitrogen properties
"""


def molar_mass():
    # kg / mol
    # ref: https://www.angelo.edu/faculty/kboudrea/periodic/structure_mass.htm
    return 2.80134e-2


def density(T=None, P=None):
    # kg / m**3
    return ideal_gas_density(molar_mass(), T, P)


def viscosity_dynamic(T=None):
    # Pa.S
    # Accurate in the range of 0degC to 600degC
    # Formula derived from tabular data (polynomial regression)
    # ref: https://www.engineeringtoolbox.com/gases-absolute-dynamic-viscosity-d_1888.html
    a = 1.8676e-09
    b = -4.8066e-06
    c = 7.0330e-03
    d = 6.1318e-02
    t = param_check(T, 'T')
    viscosity_dynamic = (a * (t**3)) + (b * (t**2)) + (c * t) + d
    return viscosity_dynamic
