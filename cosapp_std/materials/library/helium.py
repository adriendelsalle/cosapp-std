from cosapp_std.materials.tools.helpers import param_check, ideal_gas_density


"""
Helium properties
"""


def molar_mass():
    # kg / mol
    # ref: https://www.angelo.edu/faculty/kboudrea/periodic/structure_mass.htm
    return 4.00260e-03


def density(T=None, P=None):
    # kg / m**3
    return ideal_gas_density(molar_mass(), T, P)


def viscosity_dynamic(T=None):
    # Pa.S
    # Accurate in the range of 0degC to 600degC
    # Formula derived from tabular data (polynomial regression)
    # ref: https://www.engineeringtoolbox.com/gases-absolute-dynamic-viscosity-d_1888.html
    a = 1.1499e-09
    b = -3.0081e-06
    c = 6.0642e-03
    d = 4.1424e-01
    t = param_check(T, 'T')
    viscosity_dynamic = (a * (t**3)) + (b * (t**2)) + (c * t) + d
    return viscosity_dynamic
