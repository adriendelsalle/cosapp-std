import numpy
from scipy.interpolate import interp1d
from cosapp_std.materials.tools.helpers import param_check, ideal_gas_density, ideal_gas_specific_heat_cp


"""
Dry air properties
"""


def molar_mass():
    # kg / mol
    # ref: https://www.engineeringtoolbox.com/molecular-mass-air-d_679.html
    return 2.89647e-2


def adiabatic_index(T=None):
    # ref: R.D. Blevins, Applied Fluid Dynamics Handbook, 1984
    temps = (0, 233.15, 298.15, 303.15, 323.15, 333.15, 353.15, 363.15, 373.15, 473.15, 573.15, 673.15, 773.15, 1273.15, 5000)
    gammas = (1.401, 1.401, 1.401, 1.4, 1.4, 1.399, 1.399, 1.398, 1.397, 1.39, 1.379, 1.368, 1.357, 1.321, 1.0627)
    gamma_set = interp1d(temps, gammas)
    gamma = float(gamma_set(param_check(T, 'T')))
    return gamma


def density(T=None, P=None):
    # kg / m**3
    return ideal_gas_density(molar_mass(), T, P)


def viscosity_dynamic(T=None):
    # Pa.S
    # Accurate in the range of -20degC to 400degC
    # ref: https://www.tec-science.com/mechanics/gases-and-liquids/viscosity-of-liquids-and-gases/
    viscosity_dynamic = 2.791e-7 * param_check(T, 'T') * numpy.exp(0.7355)
    return viscosity_dynamic


def ideal_gas_specific_heat(T=None):
    # J / kg.K
    # Isobaric specific heat capacity: valid for temperature range 273K - 1800K, maximum error 0.72%
    # Coefficients from B. G. Kyle, Chemical and Process Thermodynamics (Englewood Cliffs, NJ: Prentice-Hall, 1984).
    SHC = {'a': 28.11, 'b': 0.1967e-2, 'c': 0.4802e-5, 'd': -1.966e-9}
    specific_heat = ideal_gas_specific_heat_cp(molar_mass(), T, SHC)
    return specific_heat


def refractive_index():
    # Given at STP
    # ref: https://en.wikipedia.org/wiki/List_of_refractive_indices
    return 1.000273
