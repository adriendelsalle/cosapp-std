from cosapp_std.materials.tools.helpers import ideal_gas_density


"""
Krypton properties
"""


def molar_mass():
    # kg / mol
    # ref: https://www.angelo.edu/faculty/kboudrea/periodic/structure_mass.htm
    return 8.38000E-02


def density(T=None, P=None):
    # kg / m**3
    return ideal_gas_density(molar_mass(), T, P)
