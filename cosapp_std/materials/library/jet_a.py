from scipy.interpolate import interp1d
from cosapp_std.materials.tools.helpers import param_check


"""
Jet-A properties
"""


def density(T=None):
    # kg / m**3
    # Linear regression with data from https://wiki.anton-paar.com/uk-en/aviation-fuels/
    density = (-7.1968e-4 * param_check(T, 'T')) + 1.02644
    return density


def specific_energy():
    # J / kg
    # ref: ASTM D1655
    return 4.28e7


def viscosity_dynamic(T=None):
    # Pa.S
    # Note, these data are a composite taken from three sources covering different temperature ranges.
    # Some data are modified with an offset, to form a continuous curve
    # Do not use if extremely precise viscosities are needed
    #
    # Refs: 293.15K - 373.15K: Viscosity Measurements of Aviation Turbine Fuels, Fortin & Laesecke 2015
    # 252.6K - 288.71K: Kerosene viscosity + 0.76 centipoise, https://cameochemicals.noaa.gov/
    # 218.15K - 250.15K: Jet A viscosity + 1.1 centipoise, https://wiki.anton-paar.com/uk-en/aviation-fuels/

    temps = (0, 218.15, 221.15, 223.15, 225.15, 227.15, 229.15, 231.15, 233.15, 235.15, 237.15, 240.15, 242.15,
    244.15, 246.15, 248.15, 250.15, 252.6, 255.4, 258.15, 260.93, 263.7, 266.48, 269.26, 272.04, 274.82,
    277.6, 280.37, 283.15, 285.93, 288.71, 293.15, 298.15, 303.15, 308.15, 313.15, 318.15, 323.15,
    328.15, 333.15, 338.15, 343.15, 348.15, 353.15, 358.15, 363.15, 368.15, 373.15, 450)
    viscosities = (1.73e-01, 1.52e-02, 1.30e-02, 1.18e-02, 1.08e-02, 9.90e-03, 9.11e-03,
    8.43e-03, 7.84e-03, 7.33e-03, 6.88e-03, 6.40e-03, 6.07e-03, 5.74e-03, 5.40e-03, 5.07e-03,
    4.79e-03, 4.50e-03, 4.18e-03, 3.89e-03, 3.63e-03, 3.39e-03, 3.18e-03, 3.00e-03, 2.82e-03,
    2.67e-03, 2.53e-03, 2.40e-03, 2.29e-03, 2.18e-03, 2.08e-03, 1.95e-03, 1.79e-03, 1.65e-03,
    1.53e-03, 1.42e-03, 1.32e-03, 1.24e-03, 1.16e-03, 1.10e-03, 1.03e-03, 9.78e-04, 9.28e-04,
    8.82e-04, 8.40e-04, 8.01e-04, 7.66e-04, 7.34e-04, 2.42e-04)

    visc_set = interp1d(temps, viscosities)
    viscosity_dynamic = float(visc_set(param_check(T, 'T')))
    return viscosity_dynamic


def specific_heat(T=None):
    # J / kg.K
    # linear regression with data from reference
    # ref: CRC Aviation Fuel Properties Handbook

    specific_heat = ((4.5e-3 * param_check(T, 'T')) + 3.0791) * 1e3
    return specific_heat


def refractive_index():
    # Given at STP, used Kerosene
    # ref: https://en.wikipedia.org/wiki/List_of_refractive_indices
    return 1.39
