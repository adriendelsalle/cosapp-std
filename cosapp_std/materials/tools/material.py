import importlib
import logging
logger = logging.getLogger(__name__)


def material(material_name=None):
    library_path = "cosapp_std.materials.library."

    if material_name is None:
        logger.warn("No material specified, using default material (dry air)")
        material_name = "air_dry"

    try:
        material_module = importlib.import_module(library_path + material_name)

    except ModuleNotFoundError:
        # module not found in library, try user input library
        logger.info(f"Using user input material {material_name}")
        material_module = importlib.import_module(material_name)

    return material_module
