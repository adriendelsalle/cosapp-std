import pytest
from cosapp_std.materials.tools.material import material


def test_Material_air():
    assert material('air_dry').density() == pytest.approx(1.224990831, rel=1e-8)


def test_Material_user_material():
    assert material('cosapp_std.materials.tools.tests.sample_material').sample_property() == 42
