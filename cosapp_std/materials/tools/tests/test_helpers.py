import pytest
from cosapp_std.materials.tools.helpers import param_check, default_condition, ideal_gas_density


def test_param_check():
    assert param_check() == None
    assert param_check(123., 'T') == 123.
    assert param_check(None, 'T') != None


def test_default_condition():
    assert default_condition('T') != None
    assert default_condition(None) == None


def test_ideal_gas_density():
    assert ideal_gas_density(2.89647e-2, 288.15, 101325) == pytest.approx(1.224990831, rel=1e-8)
