from cosapp.ports import Port


class ThermalPort(Port):
    """
    Thermal port containing variables temperature and heat flow rate

     Variables:
    ----------
    - T: Current Temperature [K]
    - q: Heat flow rate to dissipate [W]
    """

    def setup(self):
        self.add_variable('T', 273.15+30., unit='K', desc='Current Temperature')
        self.add_variable('q', 0.0, unit='W', desc='Heat Flow rate to dissipate in Joules/s')
