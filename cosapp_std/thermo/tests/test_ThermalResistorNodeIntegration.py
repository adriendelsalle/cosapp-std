from cosapp.systems import System
from cosapp_std.thermo.systems.thermalResistor import ThermalResistor
from cosapp_std.thermo.systems.thermalNode import ThermalNode
from cosapp.drivers import NonLinearSolver, NonLinearMethods
import pytest

class SerialThermoResistors(System):

    def setup(self):
        self.add_child(ThermalResistor('Resistor1'), pulling='thermal_in')
        self.add_child(ThermalResistor('Resistor2'))
        self.add_child(ThermalResistor('Resistor3'), pulling='thermal_out')
        self.connect(self.Resistor1.thermal_out, self.Resistor2.thermal_in)
        self.connect(self.Resistor2.thermal_out, self.Resistor3.thermal_in)
        self.exec_order = ["Resistor1", "Resistor2", "Resistor3"]
    
    def compute(self):
        pass


class ParallelThermoResistors(System):

    def setup(self):
        R1  = self.add_child(ThermalResistor('Resistor1'), pulling='thermal_in')
        R21 = self.add_child(ThermalResistor('Resistor21'))
        R22 = self.add_child(ThermalResistor('Resistor22'))
        R3  = self.add_child(ThermalResistor('Resistor3'), pulling='thermal_out')

        # Add nodes
        ThermalNode.make(self, 'node_split',
            incoming = [R1.thermal_out],
            outgoing = [R21.thermal_in, R22.thermal_in],
        )
        ThermalNode.make(self, 'node_join',
            incoming = [R21.thermal_out, R22.thermal_out],
            outgoing = [R3.thermal_in],
        )

        self.exec_order = [
            "Resistor1",
            "node_split",
            "Resistor21", "Resistor22",
            "node_join",
            "Resistor3",
        ]
    def compute(self):
        pass

@pytest.fixture
# System Setup
def single_calcSerial():
    system_resistor = SerialThermoResistors('SerialThermoResistors')
    system_resistor.thermal_in.T = 273.15 + 150.
    system_resistor.thermal_in.q = 140.
    thermolambda = 0.07
    A = 1.
    L = 0.03
    system_resistor.Resistor1.R = L / (thermolambda * A)
    thermolambda = 0.7
    A = 1.0
    L = 0.1
    system_resistor.Resistor2.R = L / (thermolambda * A)
    thermolambda = 0.07
    A = 1.
    L = 0.03
    system_resistor.Resistor3.R = L / (thermolambda * A)
    solver = system_resistor.add_driver(NonLinearSolver('solver', method=NonLinearMethods.POWELL, verbose=True))    
    case = solver.runner
    return system_resistor


@pytest.fixture
# System Setup
def single_calcParallel():
    system_resistor = ParallelThermoResistors('ParallelThermoResistors')
    system_resistor.thermal_in.T = 273.15 + 150.
    system_resistor.thermal_in.q = 140.
    thermolambda = 0.07
    A = 1.
    L = 0.03
    system_resistor.Resistor1.R = L / (thermolambda * A)
    thermolambda = 0.7
    A = .5
    L = 0.1
    system_resistor.Resistor21.R = L / (thermolambda * A)
    thermolambda = 0.7
    A = .5
    L = 0.1
    system_resistor.Resistor22.R = L / (thermolambda * A)
    thermolambda = 0.07
    A = 1.
    L = 0.03
    system_resistor.Resistor3.R = L / (thermolambda * A)
    solver = system_resistor.add_driver(NonLinearSolver('solver', method=NonLinearMethods.POWELL, verbose=True))
    case = solver.runner
    return system_resistor

def test_single_calcSerial(single_calcSerial):
    single_calcSerial.run_drivers()
    assert single_calcSerial.thermal_in.T == pytest.approx(423.15)
    assert single_calcSerial.thermal_in.q == pytest.approx(140.)
    assert single_calcSerial.Resistor1.thermal_out.T == pytest.approx(363.15)
    assert single_calcSerial.Resistor1.thermal_out.q == pytest.approx(140.)
    assert single_calcSerial.Resistor2.thermal_out.T == pytest.approx(343.15)
    assert single_calcSerial.Resistor2.thermal_out.q == pytest.approx(140.)
    assert single_calcSerial.Resistor3.thermal_out.T == pytest.approx(283.15)
    assert single_calcSerial.Resistor3.thermal_out.q == pytest.approx(140.)

def test_single_calcParallel(single_calcParallel):
    single_calcParallel.run_drivers()
    assert single_calcParallel.thermal_in.T == pytest.approx(423.15)
    assert single_calcParallel.Resistor1.thermal_out.T == pytest.approx(363.15)
    assert single_calcParallel.Resistor1.thermal_out.q == pytest.approx(140.)
    assert single_calcParallel.Resistor21.thermal_out.T == pytest.approx(343.15)
    assert single_calcParallel.Resistor21.thermal_out.q == pytest.approx(70.)
    assert single_calcParallel.Resistor22.thermal_out.T == pytest.approx(343.15)
    assert single_calcParallel.Resistor22.thermal_out.q == pytest.approx(70.)
    assert single_calcParallel.node_join.thermal_out0.T == pytest.approx(343.15)
    assert single_calcParallel.node_join.thermal_out0.q == pytest.approx(140.)
    assert single_calcParallel.Resistor3.thermal_out.T == pytest.approx(283.15)
    assert single_calcParallel.node_split.sum_q_out == pytest.approx(140.)
    assert single_calcParallel.node_split.thermal_out0.q == pytest.approx(70.)
    assert single_calcParallel.node_join.sum_q_out == pytest.approx(140.)
    assert single_calcParallel.node_join.thermal_in0.q == pytest.approx(70.)
    assert single_calcParallel.node_join.thermal_in1.q == pytest.approx(70.)
