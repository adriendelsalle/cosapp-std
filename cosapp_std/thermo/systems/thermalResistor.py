from cosapp.systems import System
from cosapp_std.thermo.ports import ThermalPort


class ThermalResistor(System):
    """Thermal Resistor component

    Attributes:
        R : float
            Thermal Resistance in K/W
        
    """

    def setup(self):
        self.add_input(ThermalPort, 'thermal_in')
        self.add_output(ThermalPort, 'thermal_out')
        self.add_inward('R', 1., unit='K/W', desc='Thermal Resistance')

    def compute(self):
        self.thermal_out.T = self.thermal_in.T - (self.thermal_in.q * self.R)
        self.thermal_out.q = self.thermal_in.q
