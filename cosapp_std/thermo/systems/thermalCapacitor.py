from cosapp.systems import System
from cosapp_std.thermo.ports import ThermalPort


class ThermalCapacitor(System):
    """Thermal Capacitor component

    Attributes:
        mass : float
            material mass in kg
        
        cp : float
            Thermal specific heat in J/(kg*K)
            
    """

    def setup(self):
        self.add_input(ThermalPort, 'thermal_in')
        self.add_output(ThermalPort, 'thermal_out')

        self.add_inward('mass', 1., unit='kg', desc='Material Mass')
        self.add_inward('cp', 1., unit='J/(kg*K)',
                        desc='Thermal specific heat')
        self.add_inward('dTdt', 0., unit='K/s', desc='heating rate')
        self.add_outward('C', 1., unit='J/K', desc='Thermal Capacity')

        self.add_transient('deltaT', der='dTdt')

    def compute(self):
        self.C = self.mass * self.cp
        # Rate of Temperature Increase
        self.dTdt = self.thermal_in.q / self.C

        self.thermal_out.T = self.thermal_in.T + self.deltaT

        # All Energy into Heat
        self.thermal_out.q = 0.
