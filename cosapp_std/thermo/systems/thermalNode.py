import numpy
from typing import List

from cosapp.systems import System
from cosapp_std.thermo.ports import ThermalPort


class ThermalNode(System):
    """System representing a thermal circuit node with
    an arbitrary number of incoming and outgoing branches.

    Thermal Nodes provide an off-design problem ensuring temperature equality
    and global heatflow balance (see 'Unknowns' and 'Equations' below).

    Constructor arguments:
    ----------------------
    - n_in [int], optional: Number of incoming branches. Defaults to 1.
    - n_out [int], optional: Number of outgoing branches. Defaults to 1.

    Properties:
    -----------
    - n_in [int]: Number of incoming branches.
    - n_out [int]: Number of outgoing branches.
    - incoming: Tuple containing all `ThermalPort` inputs.
    - outgoing: Tuple containing all `ThermalPort` outputs.

    Unknowns:
    ---------
    - n_out heat rate fractions (one per outgoing branch), if n_out > 1.

    Equations:
    ----------
    - (n_in - 1) temperature equality conditions for incoming branches.
    - 1 total heat rate balance equation, if n_out > 1.
    """
    def setup(self, n_in=1, n_out=1):
        """Node constructor.

        Arguments:
        -----------
        - n_in [int], optional: Number of incoming branches. Defaults to 1.
        - n_out [int], optional: Number of outgoing branches. Defaults to 1.
        """
        self.add_property('n_in', int(n_in))
        self.add_property('n_out', int(n_out))

        if min(self.n_in, self.n_out) < 1:
            raise ValueError("Node needs at least one incoming and one outgoing branch")

        self.add_property('incoming',
            tuple(
                self.add_input(ThermalPort, f"thermal_in{i}")
                for i in range(self.n_in)
            )
        )
        self.add_property('outgoing',
            tuple(
                self.add_output(ThermalPort, f"thermal_out{i}")
                for i in range(self.n_out)
            )
        )

        if self.n_out > 1:  # unnecessary otherwise
            self.add_inward('q_frac',
                value = numpy.full(self.n_out, 1.0 / self.n_out),
                desc = f"Heat rate fractions distributed to outgoing branches",
                limits = (0, 1),
            )
            self.add_unknown('q_frac', lower_bound=0, upper_bound=1)
            self.add_equation('sum(q_frac) == 1', name='Heat rate balance')

        for i in range(1, self.n_in):   # case where node is 'joiner'
            self.add_equation(f'thermal_in{i}.T == thermal_in0.T')

        self.add_outward('T', 0., unit='K', desc='Actual node temperature')
        self.add_outward('sum_q_in', 0., unit='W', desc='Sum of all incoming heat rates')
        self.add_outward('sum_q_out', 0., unit='W', desc='Sum of all outgoing heat rates')

    def compute(self):
        # Sum of incoming heat rates
        self.sum_q_in = q = sum(port.q for port in self.incoming)

        # Output temperature
        self.T = T = numpy.mean([port.T for port in self.incoming])

        # Heat rate distribution
        try:
            q_frac = self.q_frac
        except AttributeError:
            q_frac = [1]
        for j, port in enumerate(self.outgoing):
            port.T = T
            port.q = q * q_frac[j]

        self.sum_q_out = q * sum(q_frac)

    @classmethod
    def make(cls, parent, name, incoming: List[ThermalPort], outgoing: List[ThermalPort], pulling=None) -> "Node":
        """Factory method making appropriate connections with parent system"""
        node = cls(name, n_in=max(len(incoming), 1), n_out=max(len(outgoing), 1))
        parent.add_child(node, pulling=pulling)
        
        for branch_thermal, node_thermal in zip(incoming, node.incoming):
            parent.connect(branch_thermal, node_thermal)
        
        for branch_thermal, node_thermal in zip(outgoing, node.outgoing):
            parent.connect(branch_thermal, node_thermal)

        return node
