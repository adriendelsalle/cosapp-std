from cosapp_std.fluid.ports import FlowPort


class FlowPortStatic(FlowPort):
    """
    Standard fluid port with static properties
    """
    def setup(self):
        super().setup()
        self.add_variable("P", 101325., unit="Pa")
        self.add_variable("T", 288.15, unit="K")
