"""Port FlowPort2"""

from cosapp.ports import Port

import copy

from ..tools.Fluids import FluidAir

import typing

_DefaultFluid = FluidAir()  # class-level variable, should only be instantiated once
_AllFluids = {'Air': _DefaultFluid}  # class-level static dict of all available fluids

class FlowPort2(Port):
    """The FlowPort2 exists as a physical fluid interface between components"""

    @staticmethod
    def registerFluid(key, fluid):
        fluid.FluidKey = key
        _AllFluids[key] = fluid
    
    def setup(self):
        """Defines variables contained in the port."""
        self._Media = None   #  ptr to the fluid library object used
        self._State = None   #  state information unique for this particular port
        self._setFluidObject(_DefaultFluid)

        self.add_variable('Fluid', 'Air', dtype=str)
        self.add_variable('Pressure', 101325., unit='Pa')
        self.add_variable('Temperature', 273.15+15., unit='K')
        self.add_variable('Enthalpy', unit='kJ/kg')
        self.add_variable('MassFlow', 0.0, unit='kg/s')

    def _setFluidObject(self, fluidobject):
        if self._Media is fluidobject:
            pass   # nothing to do
        else:
            # initialize the state collection
            # (not sure how well this works for auto-propagation of _Media object at runtime)
            self._Media = fluidobject
            self._State = copy.deepcopy(self._Media.getStateCollection()) 

    def retrieveFluid(self):
        return self._Media
    
    def copyFromPort(self, rhsPort):
        self._setFluidObject(rhsPort._Media)
        self._State = copy.deepcopy(rhsPort._State)
        self.MassFlow = rhsPort.MassFlow

    @property
    def Fluid(self) -> str:
        return self._Media.FluidKey

    @Fluid.setter
    def Fluid(self, value):
        afluidobject = _AllFluids[value]
        self._setFluidObject( afluidobject )

    @property
    def Pressure(self):
        return self._Media.getPressure(self._State)
    
    @Pressure.setter
    def Pressure(self, value):
        self._Media.setStateFromEnthalpy(self._State, value, self.Enthalpy, None)
        
    @property
    def Temperature(self):
        return self._Media.getTemperature(self._State)

    @Temperature.setter
    def Temperature(self, value):
        self._Media.setStateFromTemperature(self._State, self.Pressure, value, None)

    @property
    def Enthalpy(self):
        """Stream enthalpy in kJ/kg"""
        return self._Media.getEnthalpy(self._State)
    
    @Enthalpy.setter
    def Enthalpy(self, value):
        self._Media.setStateFromEnthalpy(self._State, self.Pressure, value, None)

    def setStateFromEnthalpy(self, p, h, xi=[]):
        self._Media.setStateFromEnthalpy(self._State, p, h, xi)

    def setStateFromTemperature(self,p, T, xi=[]):
        self._Media.setStateFromTemperature(self._State, p, T, xi)

    def ReferenceTemperature(self):
        """Temperature (K) at enthalpy (and energy) are zero"""
        return self._Media.ReferenceTemperature

    @property
    def VolumetricFlow(self):
        w = self.MassFlow
        density = self.calculateDensity()
        return w / density


    #@property
    #def InternalEnergy(self):
    #    """Internal energy"""
    #    return self._u

    def calculateDensity(self):
        """Calculates density [kg/m**3] from state"""
        return self._Media.getDensity(self._State)

    def calculateInternalEnergy(self):
        """Calculates specific internal energy, kJ/kg"""
        return self._Media.getInternalEnergy(self._State)

    def calculateThermalConductivity(self):
        """Calculates thermal conductivity, k in kW/m/K"""
        return self._Media.getThermalConductivity(self._State)
    
    def calculateDynamicViscosity(self):
        """ Calculates dynamic viscosity, mu, in Pa*s"""
        return self._Media.getDynamicViscosity(self._State)

    def calculateCp(self):
        """ Calculates specific heat at constant pressure, cp, in kJ/kg/K"""
        return self._Media.getCp(self._State)

    def calculateCv(self):
        """ Calculates specific heat at constant volume, cv, in kJ/kg/K"""
        return self._Media.getCv(self._State)

    def calculateGasConstant(self):
        return self._Media.getGasConstant(self._State)

    def calculateSpecificHeatRatio(self):
        """ Calculates the ratio of specific heats cp/cv"""
        return self._Media.getSpecificHeatRatio(self._State)
    
    def calculatePrandtlNumber(self):
        """ Calculates the Prandtl number, momentum/thermal diffusivity, cp*mu/k """
        return self._Media.getPrandtlNumber(self._State)

    def calculateCoefficientOfThermalExpansion(self):
        """ Calculates coefficient of thermal expansion, 1/K"""
        return self._Media.getCoefficientOfThermalExpansion(self._State)

    def getPhase(self):
        """ Returns the phase of the state.  S=solid, L=liquid, 2=two-phase liquid/gas, G=gas  """
        return self._Media.getPhase(self._State)

    #def setStateFromInternalEnergy(self, p, u):
    #    self.Pressure = p
    #    self._h = self._Media.calculateEnthalpyFromTemperature(p, T, None)

    @staticmethod
    def outputFlowStreamTable(arrNames,arrFP,hasHeader=True,props=['MassFlow','VolumetricFlow','Pressure','Temperature'],\
                                    baseunits=['kg/s','m**3/s','Pa','degK'], \
                                    userunits=['lbm/min','ft**3/min','psi','degF'], showDiff=True):
        """
            Outputs a nicely-formatted HTML table for ipython/Jupyter.

            It takes an array of names and corresponding array of flowports (must be same size).
            By default, it will display the mass flow, volumetric flow,  pressure and temperature.
            Unit conversion is supported by altering the userunits input.

            Example:

            outputFlowStreamTable(['Inlet','CompOut','TurbIn'],[s.In.f_in,s.Comp.f_out,s.Turb.f_out])
        """
        import cosapp.ports.units as units
        from IPython.core.display import display, HTML

        header = ""
        if hasHeader:
            header = "<table><tr><th>Name</th>"
            for prop,userunit in zip(props,userunits):
                header += f"<th>{prop}, {userunit}</th>"
            header += "</tr>"
        
        lastFP = None
        allrowshtml = []

        def _makeRowHTML(aName: str, aFP: object, lastDP: object, doShowDiff: bool, extrastyle: str, props, baseunits,userunits) -> tuple[str,bool]:
            rowhtml = [ f"<tr style=\"{extrastyle}\"><td>{'to '+aName if (doShowDiff and lastDP) else aName}</td>" ]
            goodvals = True if (aFP.Pressure > 0.0 and aFP.Temperature > 0.0) else False
            if goodvals:
                # We have good values
                for aProp,baseunit,userunit in zip(props,baseunits,userunits):
                    val = units.convert_units(getattr(aFP,aProp),baseunit,userunit)
                    if lastFP and doShowDiff:
                        val -= units.convert_units(getattr(lastFP,aProp),baseunit,userunit)
                        if abs(val) < 1e-8:
                            rowhtml.append(f"<td></td>")
                        else:
                            rowhtml.append(f"<td>{val:+.3f}</td>")
                    else:
                        rowhtml.append(f"<td>{val:.3f}</td>")              
            else:
                # Does not contain good values, so write out empty cells
                for aprop in props:
                    rowhtml.append("<td></td>")

            return "".join(rowhtml) + "</tr>", goodvals

        # main list
        for aName, aFP in zip(arrNames,arrFP):
            rowhtmlstr, goodvals = _makeRowHTML(aName, aFP, lastFP, showDiff, "", props, baseunits, userunits)
            if goodvals and showDiff:
                lastFP = aFP
            else:
                lastFP = None
            allrowshtml.append(rowhtmlstr)

        
        # if we are showing diffs, it is nice to also show the final line
        if showDiff:
            rowhtmlstr, goodvals = _makeRowHTML(arrNames[-1], arrFP[-1], None, False, 
                                                "border-top: 1px solid #f00", props, baseunits, userunits)
            #allrowshtml.append(f"<tr><td colspan=\"{len(props)+1}\" /></tr>")
            allrowshtml.append(rowhtmlstr)

        footer = ""
        if hasHeader:
            footer = "</table>"

        strHtml = header + "\n".join(allrowshtml) + footer


        display(HTML(strHtml))


