from cosapp_std.fluid.ports import FlowPort


class FlowPortTotal(FlowPort):
    """
    Standard fluid port with total/stagnation properties
    """
    def setup(self):
        Tt = 288.15
        cp = 1004.
        super().setup()
        self.add_variable("Pt", 101325., unit="Pa")
        self.add_variable("Ht", cp * Tt, unit="J/kg")
