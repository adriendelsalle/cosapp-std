from cosapp_std.fluid.ports import FlowPortTotal


class FlowRotPort(FlowPortTotal):
    """
    Standard port for fluid rotating properties
    """
    def setup(self):
        super().setup()
        self.add_variable("alpha", 0., unit="rad", desc='absolute mean angle')
