import pytest
from cosapp_std.fluid.systems import TubeAero
from cosapp_std.fluid.systems import TubeAero2


@pytest.fixture
def tube():
    tube = TubeAero('tube')
    tube.K = 0.15
    tube.heat = 1.0
    tube.fl_in.fluid = 'air'
    tube.fl_in.W = 1.0
    tube.fl_in.Pt = 200000.
    tube.fl_in.Ht = 300000.
    return tube


def test_TubeAero_run_once(tube):
    tube.run_once()
    fluid_in = tube.fl_in
    fluid_out = tube.fl_out
    assert fluid_out.fluid == fluid_in.fluid
    assert fluid_out.W == fluid_in.W
    assert fluid_out.Pt == fluid_in.Pt - tube.dP
    assert fluid_out.Ht == fluid_in.Ht + tube.dH
    assert tube.dP == 0.15
    assert tube.dH == 1.0



def test_TubeAero2_run_once():

    tube2 = TubeAero2('tube')
    tube2.K = 0.15
    tube2.heat = 1.0
    tube2.fl_in.Fluid = 'Air'
    tube2.fl_in.MassFlow = 1.0
    tube2.fl_in.Pressure = 200000.
    tube2.fl_in.Enthalpy = 0.0

    tube2.run_once()
    fluid_in = tube2.fl_in
    fluid_out = tube2.fl_out
    assert fluid_out.Fluid == fluid_in.Fluid
    assert fluid_out.MassFlow == fluid_in.MassFlow
    assert fluid_out.Pressure == fluid_in.Pressure - tube2.dP
    assert fluid_out.Enthalpy == fluid_in.Enthalpy + tube2.dH
    assert tube2.dP == 0.15
    assert tube2.dH == 1.0



def test_TubeAero2_withwater():
    from cosapp_std.fluid.tools.Fluids import FluidLiquidWater
    from cosapp_std.fluid.ports.FlowPort2 import FlowPort2
    FlowPort2.registerFluid('water',FluidLiquidWater())

    tube2 = TubeAero2('tube')
    tube2.K = 0.15
    tube2.heat = 1.0
    tube2.fl_in.Fluid = 'water'
    tube2.fl_in.MassFlow = 1.0
    tube2.fl_in.Pressure = 200000.
    tube2.fl_in.Enthalpy = 0.0

    tube2.run_once()
    fluid_in = tube2.fl_in
    fluid_out = tube2.fl_out
    assert fluid_out.Fluid == fluid_in.Fluid
    assert fluid_out.MassFlow == fluid_in.MassFlow
    assert fluid_out.Pressure == fluid_in.Pressure - tube2.dP
    assert fluid_out.Enthalpy == fluid_in.Enthalpy + tube2.dH
    assert tube2.dP == 0.15
    assert tube2.dH == 1.0
