import pytest
from cosapp.drivers import RungeKutta
from cosapp_std.elec.systems import Capacitor


@pytest.fixture
def capacitor():
    capacitor = Capacitor('capacitor')
    return capacitor


def test_Capacitor_run_once(capacitor):
    capacitor.elec_in.V = 5.
    capacitor.elec_in.I = 0.01
    capacitance = 0.4
    capacitor.C = capacitance
    capacitor.run_once()
    elec_in = capacitor.elec_in
    elec_out = capacitor.elec_out
    assert elec_out.I == elec_in.I
    assert elec_out.V == elec_in.V


def test_Capacitor_temporal(capacitor):
    voltage = 5.
    current = 1.
    time = 0.5
    capacitance = 0.4
    capacitor.C = capacitance
    capacitor.elec_in.V = voltage
    capacitor.elec_in.I = current
    rk = capacitor.add_driver(RungeKutta(order=4))
    rk.time_interval = (0, time)
    rk.dt = 0.01
    capacitor.run_drivers()
    elec_in = capacitor.elec_in
    elec_out = capacitor.elec_out
    Q = (current * time)
    assert capacitor.Q == pytest.approx(Q, rel=1e-14)
    assert capacitor.deltaV == pytest.approx(Q / capacitance, rel=1e-14)
    assert elec_out.I == elec_in.I
    assert elec_out.V == pytest.approx(elec_in.V - (Q / capacitance), rel=1e-14)


@pytest.mark.parametrize("C, expected", [
    (0, 0),
    (1e-2, 1e-2),
    (1, 1),
    (-1, 1),
])
def test_Capacitor_setup(C, expected):
    capa = Capacitor('capa', C=C)
    assert isinstance(capa.C, float)
    assert capa.C == expected
