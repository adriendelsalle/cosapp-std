import pytest
from contextlib import nullcontext as does_not_raise
from cosapp.systems.system import System
from cosapp_std.elec.systems import Diode
from cosapp_std.elec.ports import ElecPort
from cosapp_std.elec.systems.diode import (
    AbstractDiodeModel,
    HyperbolicDiode,
    PiecewiseDiode,
    ShockleyDiode,
)


@pytest.fixture
def factory():
    def factory_impl(model):
        diode = Diode('diode', model=model)
        diode.elec_in.I = 0.1
        return diode
    return factory_impl


@pytest.mark.parametrize(
    "model, expectation",
    [
        (ShockleyDiode, does_not_raise()),
        (PiecewiseDiode, does_not_raise()),
        (HyperbolicDiode, does_not_raise()),
        (System, pytest.raises(ValueError)),
        (float, pytest.raises(ValueError)),
        ('hyperbolic', pytest.raises(TypeError)),
        (None, pytest.raises(TypeError)),
        (0, pytest.raises(TypeError)),
    ],
)
def test_Diode_setup(model, expectation):
    with expectation:
        Diode('diode', model=model)


@pytest.mark.parametrize("cls", [
    HyperbolicDiode,
    PiecewiseDiode,
    ShockleyDiode,
])
def test_Diode_api(cls):
    """Test Diode interface for all available models
    """
    diode = Diode('diode', model=cls)

    assert 'elec_in' in diode.inputs
    assert 'elec_out' in diode.outputs

    assert set(diode.inwards) == set()
    assert set(diode.outwards) == {'Rv'}

    assert isinstance(diode.elec_in, ElecPort)
    assert isinstance(diode.elec_out, ElecPort)
    assert isinstance(diode.Rv, float)

    assert list(diode.children) == ['model']
    assert isinstance(diode.model, AbstractDiodeModel)
    assert isinstance(diode.model, cls)


@pytest.mark.parametrize("model, Vin, expected_Vout", [
    (ShockleyDiode, -10., pytest.approx(-1e12, rel=1e-11)),
    (ShockleyDiode, -1., pytest.approx(-1e11 - 1, rel=1e-11)),
    (ShockleyDiode, -0.001, pytest.approx(-2635522486.121, rel=1e-11)),
    (ShockleyDiode, -0.0, pytest.approx(-2585155632.49555, rel=1e-11)),
    (ShockleyDiode, 1., pytest.approx(0.9999984124022580, rel=1e-11)),
    (ShockleyDiode, -10., pytest.approx(-1e12 - 10, rel=1e-11)),
    (PiecewiseDiode, -10., pytest.approx(-1e9 - 10, rel=1e-11)),
    (PiecewiseDiode, 0., pytest.approx(-1e9, rel=1e-11)),
    (PiecewiseDiode, 10., pytest.approx(10., rel=1e-11)),
    (HyperbolicDiode, -10., pytest.approx(-1e9 - 10, rel=1e-11)),
    (HyperbolicDiode, -1., pytest.approx(-999999959.600625, rel=1e-11)),
    (HyperbolicDiode,  0., pytest.approx(-999088948.805599, rel=1e-11)),
    (HyperbolicDiode,  1., pytest.approx(-47425872.177567, rel=1e-11)),
    (HyperbolicDiode,  10., pytest.approx(10, rel=1e-11)),
])
def test_Diode_run_once(factory, model, Vin, expected_Vout):
    diode = factory(model)
    diode.elec_in.V = Vin
    diode.run_once()
    elec_out = diode.elec_out
    assert elec_out.I == diode.elec_in.I
    assert elec_out.V == expected_Vout


@pytest.mark.parametrize("Vin, data, expected_Vout", [
    (-10., dict(), pytest.approx(-1e12)),
    (-1., dict(), pytest.approx(-1e11 - 1, rel=1e-11)),
    (-0.001, dict(), pytest.approx(-2635522486.121)),
    (-0.0, dict(), pytest.approx(-2585155632.49555)),
    (-10., dict(), pytest.approx(-1e12 - 10)),
    (1., dict(), pytest.approx(0.9999984124022580, rel=1e-11)),
    (0.8, dict(), pytest.approx(0.79709141095)),
    (0.8, dict(n=0.7), pytest.approx(0.8)),
    (0.8, dict(n=0.7, T=500.15), pytest.approx(0.5561894181438932, rel=1e-12)),
])
def test_ShockleyDiode_run_once(factory, Vin, data, expected_Vout):
    """Test Shockley model with specific parameters.
    """
    diode = factory(ShockleyDiode)
    # Set model parameters
    for var, value in data.items():
        diode.model[var] = value
    diode.elec_in.V = Vin
    diode.run_once()
    assert diode.elec_out.I == diode.elec_in.I
    assert diode.elec_out.V == expected_Vout
