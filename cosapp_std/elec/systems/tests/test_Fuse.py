import pytest
import pandas as pd
from cosapp.drivers import RungeKutta
from cosapp.drivers.time.scenario import Interpolator
import cosapp.recorders as recorders
from cosapp_std.elec.systems import Fuse


@pytest.fixture
def fuse():
    fuse = Fuse('fuse')
    fuse.elec_in.V = 5.
    fuse.I = 1.
    return fuse


def test_Fuse_run_once(fuse):
    fuse.elec_in.I = 2.0
    fuse.run_once()
    elec_in = fuse.elec_in
    elec_out = fuse.elec_out
    assert fuse.blownState == 1
    assert elec_out.I == 0.
    assert elec_out.V == elec_in.V - (elec_in.I * fuse.bR)


@pytest.mark.parametrize("no, bs, eoi, eov", [
    (0, 0, 0., 5.),
    (1, 0, 1., 5.),
    (2, 1, 0.0, -1.9999999995E+10),
])
def test_Fuse_transient(fuse, no, bs, eoi, eov):
    time = 1.
    currents = [[0, 0], [.5, 2], [1, 0]]
    fusedriver = fuse.add_driver(RungeKutta(order=3))
    fusedriver.set_scenario(values = {'elec_in.I': Interpolator(currents)})
    fusedriver.time_interval = (0, time)
    fusedriver.dt = 0.25
    rec = fusedriver.add_recorder(recorders.DataFrameRecorder(includes=['blownState', 'elec_out.I', 'elec_out.V']))
    fuse.run_drivers()
    data = pd.DataFrame(rec.export_data())
    assert data['elec_out.V'][no] == pytest.approx(eov, rel=1e-14)
    assert data['elec_out.I'][no] == pytest.approx(eoi, rel=1e-14)
    assert data['blownState'][no] == bs
