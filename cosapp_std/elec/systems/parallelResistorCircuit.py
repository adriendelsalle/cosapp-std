from cosapp.systems import System
from cosapp_std.elec.ports import ElecPort
from cosapp_std.elec.systems import Node, Resistor


class ParallelResistorCircuit(System):
    """Model of a circuit with `nP` resistors in parallel
    followed by `nS` resistors in series.

    Constructor arguments:
    ----------------------
    - nP [int]: Number of parallel resistors
    - nS [int]: Number of series resistors
    """
    def setup(self, nP, nS):

        self.add_property('nP', max(int(nP), 1))
        self.add_property('nS', max(int(nS), 0))

        self.add_property('parallel_resistors',
            tuple(
                self.add_child(Resistor(f'R_P{i}', R=100))
                for i in range(self.nP)
            )
        )
        self.add_property('series_resistors',
            tuple(
                self.add_child(Resistor(f'R_S{i}', R=100))
                for i in range(self.nS)
            )
        )
        Rp = self.parallel_resistors
        Rs = self.series_resistors

        Node.make(self, 'N1',
            incoming = [],
            outgoing = [R.elec_in for R in Rp],
            pulling = {'elec_in0': 'elec_in'},
        )

        if len(Rs) > 0:
            # Add second node, connected to first series resistor
            Node.make(self, 'N2',
                incoming = [R.elec_out for R in Rp],
                outgoing = [Rs[0].elec_in],
            )
            for i in range(len(Rs) - 1):
                self.connect(Rs[i].elec_out, Rs[i + 1].elec_in)
        
            self.add_output(ElecPort, 'elec_out')
            self.connect(Rs[-1].elec_out, self.elec_out)  # manual pulling

        else:
            # Add second node, pulling up output elec port
            Node.make(self, 'N2',
                incoming = [R.elec_out for R in Rp],
                outgoing = [],
                pulling = {'elec_out0': 'elec_out'},
            )

        self.add_outward('deltaV', 0.0, unit="V", desc="Overall potential difference")
        self.add_outward('R', 0.0, unit="ohm", desc="Overall circuit resistance")

        # Specify exec order:
        self.exec_order = ['N1'] + [R.name for R in Rp] + ['N2'] + [R.name for R in Rs]

    def compute(self):
        self.deltaV = deltaV = self.elec_in.V - self.elec_out.V
        self.R = deltaV / self.elec_in.I
