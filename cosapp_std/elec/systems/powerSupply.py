from cosapp.systems import System
from cosapp_std.elec.ports import ElecPort


class PowerSupply(System):

    def setup(self, V):

        self.add_input(ElecPort, 'elec_in')
        self.add_output(ElecPort, 'elec_out')
        self.add_inward('V', V, unit='V', desc='Power supply voltage')
        self.add_inward('I', 0., unit='A', desc='Power supply current')
        self.add_unknown('I')

        self.add_outward('Vin', 0.)
        self.add_equation('Vin == 0', name='I')

    def compute(self):
        self.elec_out.V = self.V
        self.Vin = self.elec_in.V
        self.elec_out.I = self.I
