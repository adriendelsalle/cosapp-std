import pytest

from cosapp.systems import System
from cosapp.drivers import NonLinearSolver
from cosapp_std.elec.systems import ParallelResistorCircuit as PRC
from cosapp_std.elec.systems import PowerSupply


@pytest.fixture
def factory():
    """Fixture returning a `ParallelResistorCircuit` system
    with `nP` parallel branches and `nS` resistors in series.
    """
    def factory_impl(nP, nS, name='circuit'):
        circuit = PRC(name, nP=nP, nS=nS)
        for res in circuit.parallel_resistors:
            res.R = 100.0
        for res in circuit.series_resistors:
            res.R = 100.0
        circuit.elec_in.I = 1.0
        circuit.elec_in.V = 0.0
        return circuit
    return factory_impl


def test_Node_Integration(factory):
    """Solve the resistance problem on a `ParallelResistorCircuit` system
    """
    circuit = factory(nP=3, nS=2)
    solver = circuit.add_driver(NonLinearSolver('solver', tol=1e-9))
    case = solver.runner
    case.set_values({
        'elec_in.V': 12.0,
    })
    case.design.add_unknown('elec_in.I').add_equation('deltaV == 12')
    circuit.run_drivers()

    parallel = circuit.parallel_resistors
    series = circuit.series_resistors
    Rp_exact = 1 / sum(1 / res.R for res in parallel)
    Rs_exact = sum(res.R for res in series)
    assert circuit.R == pytest.approx(Rp_exact + Rs_exact)
    assert (circuit.N1.V - circuit.N2.V) / circuit.elec_in.I == pytest.approx(Rp_exact)
    assert (circuit.N2.V - circuit.elec_out.V) / circuit.elec_in.I == pytest.approx(Rs_exact)

    assert circuit.elec_in.I == pytest.approx(0.05142857143)
    assert circuit.elec_out.I == pytest.approx(circuit.elec_in.I)
    assert circuit.N2.V == pytest.approx(10.2857142857143)
    assert circuit.R_S0.elec_out.V == pytest.approx(5.142857142857142)
    assert circuit.R_S1.elec_out.V == pytest.approx(0, abs=1e-9)

    for elec_in in circuit.N2.incoming:
        assert elec_in.V == pytest.approx(circuit.N2.V)


@pytest.mark.parametrize("nP", [1, 2, 8])
@pytest.mark.parametrize("nS", [0, 1, 2])
@pytest.mark.parametrize("deltaV", [1, -12.34])
def test_ParallelResistorCircuit(factory, nP, nS, deltaV):
    """Solve the resistance problem on a `ParallelResistorCircuit` system
    """
    circuit = factory(nP, nS)
    solver = circuit.add_driver(NonLinearSolver('solver', tol=1e-9))
    case = solver.runner
    case.set_values({
        'elec_in.V': 0.0,  # boundary condition
    })
    case.design.add_unknown('elec_in.I').add_equation(f"deltaV == {deltaV}")
    circuit.run_drivers()

    assert solver.problem.shape == (nP + 1 if nP > 1 else 1,) * 2

    parallel = circuit.parallel_resistors
    series = circuit.series_resistors
    Rp_exact = 1 / sum(1 / res.R for res in parallel)
    Rs_exact = sum(res.R for res in series)
    assert circuit.R == pytest.approx(Rp_exact + Rs_exact)
    assert (circuit.N1.V - circuit.N2.V) / circuit.elec_in.I == pytest.approx(Rp_exact)
    assert (circuit.N2.V - circuit.elec_out.V) / circuit.elec_in.I == pytest.approx(Rs_exact)
    assert circuit.deltaV == pytest.approx(deltaV)

    for elec_in in circuit.N2.incoming:
        assert elec_in.V == pytest.approx(circuit.N2.V)


def test_ParallelResistorCircuit_with_PSU(factory):
    """Combination of `ParallelResistorCircuit` and `PowerSupply`
    """
    # Assemble top-level system
    top = System('top')
    prc = factory(3, 2, name='prc')
    psu = PowerSupply('psu', V=12.0)
    top.add_child(psu)
    top.add_child(prc)
    top.connect(psu.elec_out, prc.elec_in)
    top.connect(prc.elec_out, psu.elec_in)
    top.psu.I = 0.1

    top.add_driver(NonLinearSolver('solver', tol=1e-9))
    top.run_drivers()

    circuit = top.prc
    parallel = circuit.parallel_resistors
    series = circuit.series_resistors
    assert psu.Vin == pytest.approx(0)
    assert psu.I == pytest.approx(0.05142857143)
    assert circuit.N2.V == pytest.approx(10.2857142857143)
    assert circuit.R_S0.elec_out.V == pytest.approx(5.142857142857142)
    assert circuit.R_S1.elec_out.V == pytest.approx(0, abs=1e-9)

    Rp_exact = 1 / sum(1 / res.R for res in parallel)
    Rs_exact = sum(res.R for res in series)
    assert circuit.R == pytest.approx(Rp_exact + Rs_exact)
    assert (circuit.N1.V - circuit.N2.V) / circuit.elec_in.I == pytest.approx(Rp_exact)
    assert (circuit.N2.V - circuit.elec_out.V) / circuit.elec_in.I == pytest.approx(Rs_exact)

    for elec_in in circuit.N2.incoming:
        assert elec_in.V == pytest.approx(circuit.N2.V)
