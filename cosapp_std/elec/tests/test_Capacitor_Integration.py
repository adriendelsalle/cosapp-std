import pytest
from cosapp.systems import System
from cosapp.drivers import NonLinearSolver, NonLinearMethods
from cosapp.drivers import RungeKutta
import cosapp.recorders as recorders
import pandas as pd
import numpy as np
from cosapp_std.elec.systems import Resistor, PowerSupply, Capacitor


V = 5.     # Supply voltage
C = 0.3    # Capacitor value
R = 4.7    # Resistor value
TS = 0.05  # Solver time interval


class rcCircuit(System):

    def setup(self):
        self.add_child(PowerSupply('PSU', V=V))
        self.add_child(Resistor('R1', R=R))
        self.add_child(Capacitor('C1', C=C))

        self.connect(self.PSU.elec_out, self.R1.elec_in)
        self.connect(self.R1.elec_out, self.C1.elec_in)
        self.connect(self.C1.elec_out, self.PSU.elec_in)


def q_t(t):
    q_exp = -t / (R * C)
    print(q_exp)
    Q = C * V * (1 - np.exp(q_exp))
    return Q


@pytest.mark.parametrize("no", [(0), (10), (20), (40), (60), (80)])
def test_Capacitor_Integration(no):
    sys_rc = rcCircuit('sys_rc')
    driver = sys_rc.add_driver(RungeKutta(order=4))
    driver.time_interval = (0, 4)
    driver.dt = TS
    rec = driver.add_recorder(recorders.DataFrameRecorder(includes=['C1.Q', 'C1.elec_out.V', 'C1.deltaV']))
    driver.add_child(NonLinearSolver('solver', method=NonLinearMethods.NR, verbose=False))
    sys_rc.run_drivers()
    data = pd.DataFrame(rec.export_data())

    q_ref = q_t(TS * no)
    dV_ref = q_ref / C

    assert data['C1.Q'][no] == pytest.approx(q_ref, rel=1e-6)
    assert data['C1.deltaV'][no] == pytest.approx(dV_ref, rel=1e-6)
