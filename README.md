# Standard component library for CoSApp

This work is licensed under the [following terms](LICENSE).

## Structure

### Root folder

- `setup.py`, `setup.cfg` and `MANIFEST.in`: Configuration of the project to be a valid Python package
- `HISTORY.md`: Changes log from one version to another
- `LICENSE`: License on which falls this work
- `.gitignore`: Filters files to be versionned
